using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
    private void Awake()
    {
        Application.targetFrameRate = 120;
    }

    public void LoadSceneTest()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
